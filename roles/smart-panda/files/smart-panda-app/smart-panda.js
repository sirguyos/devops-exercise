var config = require('./config.json');
var express = require('express');
var fs = require('fs');
var app = express();

var postReqCounter = 0;

app.get('/', function (req, res) {
    var html =  '<h1>'+postReqCounter+'</h1>'+
        '<form action="/" method="post">'+
            '<input type="submit" value="Run post">'+
        '</form>';
    res.send('<html><body>'+html+'</body></html>');
})

app.post("/", function(req, res) {
    postReqCounter++;
});

var server = app.listen(config.port, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})