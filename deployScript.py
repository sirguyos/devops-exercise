# didn't finish the script
import sys
from subprocess import call
hosts = sys.argv[1]
import os

# help section
if hosts[0] == '-' and hosts[1] == '-':
    if hosts[2:] == 'help':
        print('deployScript help:')
        print('------------------------')
        print('')
        print('to run deployment for all hosts use:')
        print('    deployScript all')
        print('')
        print('to run deployment for specific host/s:')
        print('    deployScript <hostname>')
        print('or')
        print('    deployScript <hostname1>,<hostname2>,...')
# all hosts
elif hosts == 'all':
    call(['ansible-playbook', '-i '+os.path.dirname(os.path.abspath(__file__))+'/hosts', 'base.yml'])
# comma seperated (assuming you have internal dns for the servers names or use the ip address)
else:
    # strip each splitted host from spaces
    hosts = [x.strip() for x in hosts.split(',')]
    hostsString = '-i '
    for host in hosts:
        hostsString += host+','
    call(['ansible-playbook', hostsString, 'base.yml'])

#coiddsss2s
# i love rita