var config = require('./config.json');
var express = require('express');
var fs = require('fs');
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, 'resources')))

function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

app.get('/', function (req, res) {
    fs.readdir(path.join(__dirname, 'resources'), (err, files) => {
        console.log(files.length);
        res.send('<html><body><img src="/p'+randomInt(1, files.length)+'.jpg"/></body></html>');
    });
})

var server = app.listen(config.port, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})
